﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bogus;
using Otus.Teaching.PromoCodeFactory.Core;
using System.Threading;
using System.Diagnostics;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public static class SampleDataFactory
    {

        static Faker<Employee> EmployeeSampler = CreateFaker();

        public static void FillTheModel(
                                        IAsyncRepository<Employee> employeesRepo,
                                        IAsyncRepository<Role> rolesRepo,
                                        int numberOfEmployees = 100)
        {
            FillTheModel(employeesRepo, rolesRepo, null, 100);
        }




        public static void FillTheModel (
                                            IAsyncRepository<Employee> employeesRepo, 
                                            IAsyncRepository<Role> rolesRepo, 
                                            IAsyncRepository<ConsoleToApiMessage> messagesRepo, 
                                            int numberOfEmployees=100)
        {
            //этот класс заполняет модель данными
            //здесь интересно то, что заполняется именно модель, а не конкртеная сущность

            if (messagesRepo!=null)
            {
                messagesRepo.AddAsync(new ConsoleToApiMessage("Message1"));
                messagesRepo.AddAsync(new ConsoleToApiMessage("Message2"));
                messagesRepo.AddAsync(new ConsoleToApiMessage("Message3"));
            }

            //Роли
            rolesRepo.AddAsync(new Role()
                            {
                                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                                Name = "Admin",
                                Description = "Администратор",
                            });

            rolesRepo.AddAsync(new Role()
                            {
                                Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                                Name = "PartnerManager",
                                Description = "Партнерский менеджер"
                            });



            //Employees админские
            List<Role> roles = rolesRepo.GetItemsListAsync().Result;

            Console.WriteLine($"Starting generation of Employees col={numberOfEmployees}");

            Stopwatch sw = new Stopwatch();
            sw.Start();

            employeesRepo.AddAsync(new Employee()
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                Roles = new List<Role>()
                {
                    roles.FirstOrDefault(x => x.Name == "Admin")
                },
                AppliedPromocodesCount = 5
            });



            employeesRepo.AddAsync(new Employee()
            {
                Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                Email = "ibis@somemail.ru",
                FirstName = "Николай",
                LastName = "Иванов",
                Roles = new List<Role>()
                {
                    roles.FirstOrDefault(x => x.Name == "Admin")
                },
                AppliedPromocodesCount = 10
            });

            //Employees обычные

            Role role01 = roles.FirstOrDefault(x => x.Name == "PartnerManager");

            for (int i=1; i<= numberOfEmployees;i++)
            {
                Employee employee = EmployeeSampler.Generate();
                employee.Roles = new List<Role>();
                employee.Roles.Add(role01);
                employeesRepo.AddAsync(employee);
            }
            sw.Stop();
            Console.WriteLine($"Generation done, {sw.ElapsedMilliseconds} ms elapsed that is {Math.Round((double)sw.ElapsedMilliseconds/numberOfEmployees, 5) } per record");
        }




        private static Faker<Employee> CreateFaker()
        {
            Random random = new Random();
            var customersFaker = new Faker<Employee>()
                .CustomInstantiator(f => new Employee()
                {
                    Id = Guid.NewGuid(),
                })
                .RuleFor(u => u.Id, (u) => Guid.NewGuid())
                .RuleFor(u => u.FirstName, (f, u) => f.Name.FirstName())
                .RuleFor(u => u.LastName, (f, u) => f.Name.LastName())
                .RuleFor(u => u.Email, (f, u) => f.Internet.Email())
                .RuleFor(u => u.AppliedPromocodesCount, (u) => random.Next(0, 20));
            
                //.RuleFor(u => u, (f, u) => f.Phone.PhoneNumber("1-###-###-####"));

            return customersFaker;
        }


        public static IEnumerable<Employee> Employees => new List<Employee>()
        {
            new Employee()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                Roles = new List<Role>()
                {
                    Roles.FirstOrDefault(x => x.Name == "Admin")  
                },
                AppliedPromocodesCount = 5
            },
            new Employee()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                Roles = new List<Role>()
                {
                    Roles.FirstOrDefault(x => x.Name == "PartnerManager")  
                },
                AppliedPromocodesCount = 10
            },
        };

        public static IEnumerable<Role> Roles => new List<Role>()
        {
            new Role()
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                Name = "Admin",
                Description = "Администратор",
            },
            new Role()
            {
                Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            }
        };

        public static Employee NewEmployee()
        {
            return EmployeeSampler.Generate();
        }
    }
       
}
