﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core
{
    public class Role : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}