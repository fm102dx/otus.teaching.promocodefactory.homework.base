﻿using Otus.Teaching.PromoCodeFactory.Core;
using System;

namespace Otus.Teaching.PromoCodeFactory.ConsoleСlient
{
    internal class Program
    {
        static void Main(string[] args)
        {
            PromoCodesConsoleManager app = new PromoCodesConsoleManager();
            app.Run();
        }

    }
}



