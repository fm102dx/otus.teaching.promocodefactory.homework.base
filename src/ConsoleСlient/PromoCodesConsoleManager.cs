﻿using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.Core;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.ConsoleСlient
{
    public class PromoCodesConsoleManager
    {
        IAsyncRepository<Employee> employeesRepo;
        IAsyncRepository<Employee> employeesRemoteRepo;
        IAsyncRepository<Role> rolesRepo;
        IAsyncRepository<ConsoleToApiMessage> messagesRepo;

        public void Run()
        {
            EFSqliteDbContext dbContext = new EFSqliteDbContext();

            //Репозитории
            employeesRepo = new EfAsyncRepository<Employee>(dbContext);
            rolesRepo = new EfAsyncRepository<Role>(dbContext);
            messagesRepo = new WebApiAsyncRepository<ConsoleToApiMessage>(@"https://promocodes.ricompany.info/api/v1/ConsoleToApiMessages");
            employeesRemoteRepo = new WebApiAsyncRepository<Employee>(@"https://promocodes.ricompany.info/api/v1/Employees");

            //инициализация локальной базы SQLITE
            employeesRepo.InitAsync(true);

            //заполняем модель
            SampleDataFactory.FillTheModel(employeesRepo, rolesRepo, 15);

            int x = employeesRepo.Count.Result;

            int y = rolesRepo.Count.Result;

            Console.WriteLine($"x={x} y={y}");

            AppMenuMaker menu = PrepareMenu();
            
            menu.Run();
        }

        public AppMenuMaker PrepareMenu()
        {
            AppMenuMaker menu = new AppMenuMaker("Приложение по генерации промокодов, консольный клиент", "");
            menu.SetWritingDevice(new AppMenuMakerWritingDevice());

            AppMenuMaker menuWorkLocally = new AppMenuMaker("Работаем локально", "");
            AppMenuMaker menuWorkByNetwork = new AppMenuMaker("Работаем c удаленным репозиторием", "");

            menu.AddMenuItemSubMenu(1, "Работать локально", menuWorkLocally);
            menu.AddMenuItemSubMenu(2, "Работать c удаленным репозиторием", menuWorkByNetwork);

            menuWorkLocally.AddMenuItemDelegate(1, "Показать всех сотрудников", () =>
            {
                employeesRepo.GetAllAsync().Result.ToList().ForEach(e => ConsoleWriter.WriteDefault(e.ToString()));
                ConsoleWriter.WriteDefault($"Total {employeesRepo.Count.Result} objects");
            });

            menuWorkLocally.AddMenuItemDelegate(2, "Выбор сотрудника", () =>
            {
                List<Employee> lst = employeesRepo.GetAllAsync().Result.ToList();


                Dictionary<int, Employee> keyValuePairs = new Dictionary<int, Employee>();

                for (int  i =0; i<lst.Count;i++)
                {
                    keyValuePairs.Add(i+1, lst[i]);
                }
                
                foreach (var x in keyValuePairs)
                {
                    ConsoleWriter.WriteDefault($"{x.Key}. {x.Value.ToString()}");
                }
                
                bool canGoOut;
                int emplNumber=-1;
                do
                {
                    ConsoleWriter.WriteDefault("");
                    ConsoleWriter.WriteDefault("Введите порядковый номер сотрудника чтобы выбрать его");
                    var s = Console.ReadLine();
                    canGoOut = Int32.TryParse(s, out emplNumber) || s=='/'.ToString();
                    if (!canGoOut) ConsoleWriter.WriteRed("Введите корректный int или '/' для выхода");

                }
                while (!canGoOut);

                if (emplNumber == -1) return;

                Guid targetGuid = keyValuePairs.GetValueOrDefault(emplNumber).Id;

                Console.WriteLine($"Seleted item guid = {targetGuid.ToString()}");
            });

            menuWorkByNetwork.AddMenuItemDelegate(1, "Отправить сообщение на веб-консоль", () =>
            {
                    ConsoleWriter.WriteDefault("Введите текстовое сообщение или '/' для выхода");
                    var s = Console.ReadLine();
                    if (s != '/'.ToString())
                    {
                       CommonOperationResult rez = messagesRepo.AddAsync(new ConsoleToApiMessage(s)).Result;
                       ConsoleWriter.WriteDefault($"Результат операции: {rez.ShrotString()}");
                    };


            });
            menuWorkByNetwork.AddMenuItemDelegate(2, "Сгенерировать запись о сотруднике", () =>
            {
                CommonOperationResult rez = employeesRemoteRepo.AddAsync(SampleDataFactory.NewEmployee()).Result;
                ConsoleWriter.WriteDefault($"Результат операции: {rez.ShrotString()}");
            });


            return menu;

        }
    }
}
