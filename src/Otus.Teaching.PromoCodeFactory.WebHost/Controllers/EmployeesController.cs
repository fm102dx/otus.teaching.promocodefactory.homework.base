﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Otus.Teaching.PromoCodeFactory.Core;

namespace OtusREST.Controllers
{

    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController : Controller
    {

        IAsyncRepository<Employee>  _repo;

        public EmployeesController(IAsyncRepository<Employee> repo)
        {
            _repo = repo;
        }


        /// <summary>
        /// Диагностическое сообщение контроллера
        /// </summary>
        [HttpGet]
        public string Get()
        {
            return "This is Employees controller!";
        }

        /// <summary>
        /// Количество объектов в репозитории (async)
        /// </summary>
        [HttpGet("count/")]
        public async Task<int> Count()
        {
            return await _repo.Count;
        }


        /// <summary>
        /// Существует ли объект с таким id (async)
        /// </summary>
        /// 
        [HttpGet("exists/{id}")]
        public async Task<bool> Exists(Guid id)
        {
            
            return await _repo.Exists(id);
        }

        /// <summary>
        /// Достать объект по id или вернуть null (async)
        /// </summary>
        /// 
        [HttpGet("GetByIdOrNull/{id}")]
        public async Task<Employee> GetByIdOrNullAsync(Guid id)
        {
            return await _repo.GetByIdOrNullAsync(id);
        }

        /// <summary>
        /// Получить список всех объектов (async)
        /// </summary>
        [HttpGet]
        [Route("getall/")]
        public async Task<IEnumerable<Employee>> GetAllAsync()
        {
            return await _repo.GetAllAsync();
        }


        /// <summary>
        /// Добавить объект (async)
        /// </summary>
        [HttpPost]
        public Task<CommonOperationResult> AddAsync([FromBody] Employee employee)
        {
            return _repo.AddAsync(employee);
        }


        /// <summary>
        /// Изменить существующий объект (async)
        /// </summary>
        [HttpPut]
        public Task<CommonOperationResult> ModifyCustomer([FromBody] Employee employee)
        {
            return _repo.UpdateAsync(employee);

        }


        /// <summary>
        /// Удалить объект с данным id (async)
        /// </summary>
        /// 
        [HttpDelete("{id}")]
        public Task<CommonOperationResult> Delete(Guid id)
        {
            return _repo.DeleteAsync(id);
        }
    }
}
