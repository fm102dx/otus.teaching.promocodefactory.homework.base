﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core;
using Otus.Teaching.PromoCodeFactory.WebHost.ViewModels;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [Route("/")]
    public class HomeController : Controller
    {

        IAsyncRepository<ConsoleToApiMessage> _messagesRepo;
        IAsyncRepository<Employee> _employessRepo;

        public HomeController (IAsyncRepository<ConsoleToApiMessage> messagesRepo, IAsyncRepository<Employee> employessRepo)
        {
            _messagesRepo = messagesRepo;
            _employessRepo = employessRepo;
        }

        [HttpGet]
        public IActionResult Index01()
        {
            var viewModel = new HomeViewModel(_employessRepo, _messagesRepo);
            return View("Index01", viewModel);
        }
    }
}
