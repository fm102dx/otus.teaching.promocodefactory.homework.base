﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Otus.Teaching.PromoCodeFactory.Core;

namespace OtusREST.Controllers
{

    [ApiController]
    [Route("api/v1/[controller]")]
    public class ConsoleToApiMessagesController : Controller
    {

        IAsyncRepository<ConsoleToApiMessage>  _repo;

        public ConsoleToApiMessagesController(IAsyncRepository<ConsoleToApiMessage> repo)
        {
            _repo = repo;
        }

        /// <summary>
        /// Диагностическое сообщение контроллера
        /// </summary>
        [HttpGet]
        public string Get()
        {
            return "This is ConsoleToApiMessages controller!";
        }

        /// <summary>
        /// Количество объектов в репозитории (async)
        /// </summary>
        [HttpGet("count/")]
        public async Task<int> Count()
        {
            return await _repo.Count;
        }

        /// <summary>
        /// Получить список всех объектов (async)
        /// </summary>
        [HttpGet]
        [Route("getall/")]
        public async Task<IEnumerable<ConsoleToApiMessage>> GetAllAsync()
        {
            return await _repo.GetAllAsync();
        }


        /// <summary>
        /// Добавить объект (async)
        /// </summary>
        [HttpPost]
        public async Task<CommonOperationResult> AddAsync([FromBody] ConsoleToApiMessage message)
        {
            return await _repo.AddAsync(message);
        }


          /// <summary>
        /// Удалить объект с данным id (async)
        /// </summary>
        /// 
        [HttpDelete("{id}")]
        public async Task<CommonOperationResult> DeleteAsync(Guid id)
        {
            return await _repo.DeleteAsync(id);
        }
    }
}
