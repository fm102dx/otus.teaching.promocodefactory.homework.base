using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.PromoCodeFactory.Core;
using Otus.Teaching.PromoCodeFactory.DataAccess;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddControllers();
                services.AddControllersWithViews();

                IAsyncRepository<Employee> employeesRepo;
                IAsyncRepository<Role> rolesRepo;
                IAsyncRepository<ConsoleToApiMessage> messagesRepo;


            EFSqliteDbContext dbContext = new EFSqliteDbContext();
            EfSqlServerDbContext efSqlServerDbContext = new EfSqlServerDbContext();

            //������������� ����
            //  employeesRepo = new EfAsyncRepository<Employee>(dbContext);
            // employeesRepo.InitAsync();


            /*
            employeesRepo = new EfAsyncRepository<Employee>(efSqlServerDbContext);
            rolesRepo = new EfAsyncRepository<Role>(efSqlServerDbContext);
            messagesRepo = new EfAsyncRepository<ConsoleToApiMessage>(efSqlServerDbContext);
            */
            
                employeesRepo = new InMemoryAsyncRepository<Employee>();
                rolesRepo = new InMemoryAsyncRepository<Role>();
                messagesRepo = new InMemoryAsyncRepository<ConsoleToApiMessage>();
            


            SampleDataFactory.FillTheModel(employeesRepo, rolesRepo, messagesRepo, 25);

            //���������� ������������ � DI
            //EF-SQLITE
            //services.AddTransient(typeof(IAsyncRepository<Employee>), (x) => new EfAsyncRepository<Employee>(new EFSqliteDbContext()));
            //services.AddTransient(typeof(IAsyncRepository<Role>), (x) => new EfAsyncRepository<Role>(new EFSqliteDbContext()));

            //in-memory
            
                        services.AddSingleton(typeof(IAsyncRepository<Employee>), (x) => employeesRepo);
                        services.AddSingleton(typeof(IAsyncRepository<Role>), (x) => rolesRepo);
                        services.AddSingleton(typeof(IAsyncRepository<ConsoleToApiMessage>), (x) => messagesRepo);
              
            //sql server
            /*
            services.AddScoped(typeof(IAsyncRepository<Employee>), (x) => new EfAsyncRepository<Employee>(efSqlServerDbContext));
            services.AddScoped(typeof(IAsyncRepository<Role>), (x) => new EfAsyncRepository<Role>(efSqlServerDbContext));
            services.AddScoped(typeof(IAsyncRepository<ConsoleToApiMessage>), (x) => new EfAsyncRepository<ConsoleToApiMessage>(efSqlServerDbContext));
            */

            services.AddOpenApiDocument(options =>
            {
                options.Title = "PromoCode Factory API Doc";
                options.Version = "1.0";
                options.Description = "descr_001";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseOpenApi();

            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });
            
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}