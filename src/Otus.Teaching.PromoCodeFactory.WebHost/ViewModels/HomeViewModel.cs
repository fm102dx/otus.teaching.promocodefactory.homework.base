﻿using Otus.Teaching.PromoCodeFactory.Core;

namespace Otus.Teaching.PromoCodeFactory.WebHost.ViewModels
{
    public class HomeViewModel
    {
        public IAsyncRepository<Employee> EmployeesRepository { get; set; }
        public IAsyncRepository<ConsoleToApiMessage> ConsoleToApiMessagesRepository { get; set; }
        public HomeViewModel (IAsyncRepository<Employee> employeesRepository, IAsyncRepository<ConsoleToApiMessage> messages)
        {
            EmployeesRepository = employeesRepository;
            ConsoleToApiMessagesRepository = messages;
        }
    }
}
